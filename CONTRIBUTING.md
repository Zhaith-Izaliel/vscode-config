# Contributing

When contributing to this repository, please first discuss the change you wish to make via issues.

Please note we have a code of conduct, please follow it in all your interactions with the project.

- [Contributing](#contributing)
  - [Coding style and processes](#coding-style-and-processes)
    - [Coding style](#coding-style)
    - [Commit message style](#commit-message-style)
    - [Branch naming and process](#branch-naming-and-process)
  - [Pull Request Process](#pull-request-process)
  - [Code of Conduct](#code-of-conduct)
    - [Our Pledge](#our-pledge)
    - [Our Standards](#our-standards)
    - [Our Responsibilities](#our-responsibilities)
    - [Scope](#scope)
    - [Enforcement](#enforcement)
    - [Attribution](#attribution)

## Coding style and processes

### Coding style

We follow strict coding style to enforce stability and simplicity to share and improve the project.
Here are the guidelines you must follow at anytime contributing to the project:

- Indent with 2 spaces, no tabs
- No trailing whitespace
- Always use snake case naming
- Document **every** functions in the following format:

```bash
#######################################
# <function description>
# Globals:
#   <global variable name>
# Arguments:
#   <argument type>: <description>
# Outputs:
#   <outputs description>
#######################################
function_name() {

}
```

- Functions must be named with leading parenthesis.
- Avoid using `eval` and use it only if necessary
- Always use `err` function to report errors to stderr
- Always use `verbose` to describe functions action in verbose mode

If any of these guidelines are broke, your contribution will be rejected.

### Commit message style

Every commit must follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) rules.

### Branch naming and process

Every branch should be named in the git-flow format: `prefix/name`. You can find the list of valid prefixes here:

- `feature`: Feature branch
- `bugfix`: Bugfix branch
- `release`: Integration branch for a new release
- `hotfix`: Hotfix branch
- `support`: Adding support for other systems (ie. Windows)

The branch develop is here to integrate modifications for releases and testing them. When testing shows no regression error, the owner will merge modifications from develop to master, thus, creating a new release.

## Pull Request Process

1. Create a branch new branch corresponding to the modification implemented. (See [Branch naming and process](#branch-naming-and-process) for naming conventions.)
2. Update the README.md with details of changes to the script, including flags and caveats.
3. Increase the version numbers in the readonly `VERSION` constant in the script. The versioning scheme we use is [SemVer](http://semver.org/).
4. Adding your changes in the CHANGELOG, titled with the new version number.
5. Your pull request will be merged by the owner after being reviewed by at least two developers.

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

- Using welcoming and inclusive language
- Being respectful of differing viewpoints and experiences
- Gracefully accepting constructive criticism
- Focusing on what is best for the community
- Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

- The use of sexualized language or imagery and unwelcome sexual attention or
  advances
- Trolling, insulting/derogatory comments, and personal or political attacks
- Public or private harassment
- Publishing others' private information, such as a physical or electronic
  address, without explicit permission
- Other conduct which could reasonably be considered inappropriate in a
  professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the project owner [here](https://aemail.com/82LL). All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. The project team is
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4,
available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/
